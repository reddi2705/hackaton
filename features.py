from utils import DatasetItem
from sklearn.feature_extraction.text import TfidfVectorizer


class FeaturesBuilder:

    def __init__(self, dataset):
        self.dataset = dataset

    def convertToFeatures(self):
        print("Converting messages to numbers...")
        vectors = self.__vectorizeDataset()
        print("TfIdf ready. Converting to features...")
        featureSet = self.__convertVectorsToFeatures(vectors)
        print("Converted to features successfully")
        return featureSet

    def __vectorizeDataset(self):
        vektorizerData = []
        for item in self.dataset:
            vektorizerData.append(item.words)
        vectorizer = TfidfVectorizer(min_df=1, max_df=0.7, sublinear_tf=True, use_idf=True, stop_words="english")
        return vectorizer.fit_transform(vektorizerData).toarray()

    def __convertVectorsToFeatures(self, vectors):
        numberOfDocuments = len(vectors)
        for i in range(numberOfDocuments):
            self.dataset[i].setFeatures(vectors[i])
        return self.dataset
