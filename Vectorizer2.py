from Parser import Parser
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import svm
from sklearn.metrics import classification_report


parser = Parser('data/dialog_posts_20170410_20170413_2.json')
words_to_process = parser.load_json()

print words_to_process

train_data = ['test', 'message', 'testovaci', 'bla', 'zprava', 'post']
train_labels = ['test', 'message', 'testovaci', 'bla', 'zprava', 'post']
test_data = []
# test_labels = ['test direct', 'post', 'testovaci', 'test', 'test', 'test', 'direct', 'test', 'direct', 'post', 'post', 'test', 'test', 'post', 'test', 'post', 'test', 'post', 'test', 'post', 'test', 'post']
test_labels = []

for word in words_to_process:
    test_data.append(word)
    test_labels.append('test')

vectorizer = TfidfVectorizer(min_df=1,
                             max_df=0.7,
                             sublinear_tf=True,
                             use_idf=True)
train_vectors = vectorizer.fit_transform(train_data)
test_vectors = vectorizer.transform(test_data)

classifier_linear = svm.SVC(kernel='linear')
classifier_linear.fit(train_vectors, train_labels)
prediction_linear = classifier_linear.predict(test_vectors)

print(classification_report(test_labels, prediction_linear))
