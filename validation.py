from sklearn.cross_validation import KFold
from tags import TagManager


class CrossValidation:

    def __init__(self, numberOfFolds, modelFactoryClass):
        self.numberOfFolds = numberOfFolds
        self.modelFactoryClass = modelFactoryClass

    def getAccuracy(self, dataset):
        accuracySum = 0.0
        kf = KFold(len(dataset), n_folds=self.numberOfFolds)
        for trainIndex, testIndex in kf:
            print("Starting another cross validation iteration...")
            train = self.__extractDatasetItems(dataset, trainIndex)
            test = self.__extractDatasetItems(dataset, testIndex)
            print("Creating model...")
            model = self.__createModel(train)
            print("Computing model performance...")
            modelAccuracy = self.__getModelAccuracy(model, test)
            print("Model accuracy: " + str(modelAccuracy))
            print("----------")
            accuracySum += modelAccuracy
        return accuracySum / float(self.numberOfFolds)

    def __extractDatasetItems(self, dataset, index):
        items = []
        for item in index:
            items.append(dataset[item])
        return items

    def __createModel(self, trainItems):
        factory = self.modelFactoryClass(TagManager())
        for item in trainItems:
            factory.addText(item)
        return factory.getModel()

    def __getModelAccuracy(self, model, testItems):
        accuracySum = 0.0
        for item in testItems:
            accuracySum += model.score(item)
        return accuracySum / float(len(testItems))
