from Parser import Parser


def getDataset(count):
    print("Loading data...")
    parser = Parser('data/posts_' + count + '.json')
    data = parser.load_json()
    print("Data loaded successfully")
    return data
