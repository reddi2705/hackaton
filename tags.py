class TagManager:

    def __init__(self):
        self.tags = []
        self.dirtyTags = False

    def addTag(self, tag):
        if tag not in self.tags:
            self.tags.append(tag)
        self.dirtyTags = True

    def addTags(self, tags):
        self.tags += tags
        self.dirtyTags = True

    def getAllTags(self):
        if self.dirtyTags:
            self.tags = list(set(self.tags))
            self.dirtyTags = False
        return self.tags

    def getTagsCount(self):
        return len(self.tags)
