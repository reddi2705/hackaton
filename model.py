from sklearn import neighbors


class Model:

    NUMBER_OF_NEIGHBOURS = 2

    def __init__(self, items, tagsManager):
        self.items = items
        self.tagsManager = tagsManager
        self.tagModels = {}
        return

    def addTag(self, tag, tagTargets):
        model = self.__createModel(tag)
        features = []
        for item in self.items:
            features.append(item.features)
        model.fit(features, tagTargets)
        self.tagModels[tag] = model

    def predict(self, item):
        tags = []
        for tag in self.tagsManager.getAllTags():
            model = self.tagModels[tag]
            prediction = model.predict([item.features])[0]
            if prediction: tags.append(tag)
        return tags

    # Use Jaccard similarity, https://en.wikipedia.org/wiki/Jaccard_index
    def score(self, item):
        expectedTags = item.tags
        actualTags = self.predict(item)
        m11 = 0
        for tag in expectedTags:
            if tag in actualTags: m11 += 1
        m10 = len(expectedTags) - m11
        m01 = len(actualTags) - m11
        return 0 if m11 + m10 + m01 == 0 else m11 / float(m11 + m10 + m01)

    def __createModel(self, tag):
        return neighbors.KNeighborsClassifier(self.NUMBER_OF_NEIGHBOURS)


class ModelFactory:

    def __init__(self, tagsManager):
        self.tagsManager = tagsManager
        self.items = []
        return

    def addText(self, item):
        self.items.append(item)
        self.tagsManager.addTags(item.tags)
        return

    def getModel(self):
        model = Model(self.items, self.tagsManager)
        tags = self.tagsManager.getAllTags()
        for i in range(self.tagsManager.getTagsCount()):
            tag = tags[i]
            tagTargets = self.__getTagTargets(tag)
            model.addTag(tag, tagTargets)
        return model

    def __getTagTargets(self, tag):
        tagTargets = []
        for item in self.items:
            tagTargets.append(1 if tag in item.tags else 0)
        return tagTargets
