# Use Jaccard similarity, https://en.wikipedia.org/wiki/Jaccard_index
def compareBinaryVectors(vector1, vector2):
    if len(vector1) is not len(vector2):
        raise Exception("Vectors haven't same length")
    length = len(vector1)
    m11 = 0
    m10 = 0
    m01 = 0
    for i in range(0, length):
        if vector1[i] is 1 and vector2[i] is 1: m11 += 1
        if vector1[i] is 1 and vector2[i] is 0: m10 += 1
        if vector1[i] is 0 and vector2[i] is 1: m01 += 1
    return 0 if m11 + m10 + m01 == 0 else m11 / float(m11 + m10 + m01)


class DatasetItem:

    def __init__(self, words, tags):
        self.words = words
        self.tags = tags

    def setFeatures(self, features):
        self.features = features
