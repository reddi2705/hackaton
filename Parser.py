import json
import nltk
import unicodedata
from utils import DatasetItem


class Parser:
    def __init__(self, json_file):
        self.data_set = []
        self.json_file = json_file

    def load_json(self):
        with open(self.json_file) as data_file:
            data = json.load(data_file)

            for post in data:
                self.data_set.append(DatasetItem(post['message'], post['tags']))
        return self.data_set

    @staticmethod
    def normalize_text(text):
        ascii_data = unicodedata.normalize('NFKD', text).encode("ascii", "ignore")

        tokens = nltk.word_tokenize(text)
        text = nltk.Text(tokens)
        words = [w.lower() for w in text]

        # return sorted(set(words))
        return words



