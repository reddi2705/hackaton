import sys
import warnings
from features import FeaturesBuilder
from dataset import getDataset
warnings.filterwarnings("ignore")
from validation import CrossValidation
from model import ModelFactory
from model2 import Model2Factory

CROSS_VALIDATION_FOLDS = 3


def usage():
    print("Count of posts missing! Use as a first parameter.")

if len(sys.argv) < 2:
    usage()
    sys.exit(1)

featuresBuilder = FeaturesBuilder(getDataset(sys.argv[1]))
crossValidation = CrossValidation(CROSS_VALIDATION_FOLDS, ModelFactory)
features = featuresBuilder.convertToFeatures()
print("----------")
print("----------")
print(crossValidation.getAccuracy(features))
