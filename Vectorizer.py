from Parser import Parser
import pprint
from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd


parser = Parser('data/dialog_posts_20170410_20170413_2.json')
# pp = pprint.PrettyPrinter()
# pp.pprint(parser.load_json())

words_to_process = parser.normalize_text(parser.list_to_str(parser.load_json()))
# words_to_process = parser.normalize_text(parser.load_json())

# tf = TfidfVectorizer(analyzer='word', ngram_range=(1, 1), min_df=0, stop_words='english')
# tfidf_matrix = tf.fit_transform(words_to_process)
# feature_names = tf.get_feature_names()
# dense = tfidf_matrix.todense()
#
# words = dense[0].tolist()[0]
#
# phrase_scores = [pair for pair in zip(range(0, len(words)), words) if pair[1] > 0]
# sorted_phrase_scores = sorted(phrase_scores, key=lambda t: t[1] * -1)
#
# for phrase, score in [(feature_names[word_id], score) for (word_id, score) in sorted_phrase_scores]:
#     print '{0: <30} {1}'.format(phrase, score)


vectorizer = TfidfVectorizer(sublinear_tf=True)
tfidf_matrix = vectorizer.fit_transform(words_to_process)
feature_names = vectorizer.get_feature_names()
dense = tfidf_matrix.todense()
# denselist = dense[0].tolist()[0]
#
# print denselist
# print feature_names
# print words_to_process
#
# df = pd.DataFrame(denselist, columns=feature_names, index=)
# s = pd.Series(df.loc['test'])
# s[s > 0].sort_values(ascending=False)[:10]