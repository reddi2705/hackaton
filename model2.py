from sklearn import neighbors

class Model2:

    NUMBER_OF_NEIGHBOURS = 3

    def __init__(self, items):
        self.items = items
        self.model = self.__createModel()
        features = []
        for item in self.items:
            features.append(item.features)
        self.model.fit(features)

    def predict(self, item):
        distances, neighbors = self.model.kneighbors([item.features])
        tags = []
        for neighbor in neighbors[0]:
            tags += self.items[neighbor].tags
        return list(set(tags))

    # Use Jaccard similarity, https://en.wikipedia.org/wiki/Jaccard_index
    def score(self, item):
        expectedTags = item.tags
        actualTags = self.predict(item)
        m11 = 0
        for tag in expectedTags:
            if tag in actualTags: m11 += 1
        m10 = len(expectedTags) - m11
        m01 = len(actualTags) - m11
        return 0 if m11 + m10 + m01 == 0 else m11 / float(m11 + m10 + m01)

    def __createModel(self):
        return neighbors.NearestNeighbors(self.NUMBER_OF_NEIGHBOURS)

class Model2Factory:

    def __init__(self, tagsManager):
        self.tagsManager = tagsManager
        self.items = []
        return

    def addText(self, item):
        self.items.append(item)
        return

    def getModel(self):
        return Model2(self.items)
